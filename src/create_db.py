#!/usr/bin/env python3
"""Module realise class Person"""
from sqlalchemy import Column, Date, BigInteger, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from engine import engine

Base = declarative_base()

class Person(Base):
    __tablename__ = 'unix_syslog_data'#'person_m_agg'

    # создание полей таблицы
    # message_type: debug(7), info(6), notice(5), warning/warn(4), error(3), crit(2), alert(1), emerg/panic(0)
    
    # key
    pk = Column(BigInteger, primary_key=True)

    # message priority
    priority_level = Column(String(10), nullable=False)

    # message priority
    priority_type = Column(BigInteger, nullable=False)

    # who send
    facility_level = Column(String(20), nullable=False)

    # message body
    message = Column(String(150), nullable=False)

    # hour
    hour = Column(BigInteger, nullable=False)

    def __repr__(self):
        return ('(pk={}, priority_level={}, priority_type={}, facility_level={}, message={}, hour={})'.format(pk, priority_level, priority_type, facility_level, message, hour))

if __name__ == '__main__':
    Base.metadata.create_all(engine)

