#!/usr/bin/spark-submit
import sys
import os
import findspark
findspark.init('/usr/lib/spark')
from pyspark import SparkContext
from datetime import datetime
from dateutil.relativedelta import relativedelta

path_from = '/user/cloudera/unix_syslog_data/'
fname = 'part-m-[0-9]*'
full_path = os.path.join(path_from, fname)
path_to = os.path.join(path_from, 'output')

def unix_signals(input_file, output_file):
    """Подсчет количества сигналов каждого типа (0-7) за каждый час"""

    sc = SparkContext(appName='Spark SQL Task')
    data = sc.textFile(input_file)
    data = data.map(lambda x: x.split(',')) \
               .map(lambda row: ( (int(row[5]) ), ( (row[4]), int(row[2]) ) )) \
               .groupByKey() \
               .map(lambda row: (row[0], (row[1], 1))) \
               .groupByKey() \
               .count() 
               .map(lambda R: '%d,%d,%d' % (R[0], R[1], R[2]))
     
    data.saveAsTextFile(output_file)

if __name__ == '__main__':
    unix_signals(full_path, path_to)

